import { Chart } from 'chart.js';
import * as extrapolate from "extrapolate";

export class LinkCard {
    cardText: string;
    chartInfo: any;
    infoType: string;
    favorited: boolean;

    chart: Chart;
    isPredicting: boolean;
    canPredict: boolean;

    constructor(cardText: string, chartInfo: any, infoType: string, canPredict: boolean, favorited: boolean = false) {
        this.cardText = cardText;
        this.chartInfo = chartInfo;
        this.infoType = infoType;
        this.favorited = favorited;
        this.canPredict = canPredict
    }

    static getDefaultFavoriteCardInfo(shortUrl: string) {
        return {
            title: "Users this week",
            type: "line",
            datasetLabel: "Visitors on your website",
            infoType: "Weekly",
            shortUrl: shortUrl
        }
    }

    static generateBaseCards(favoritedCardTitles: string[], data: any): LinkCard[] {
        var baseCards = []

        //Weekly
        baseCards.push(
            this.generateCard({
                title: "Users this week",
                type: "line",
                datasetLabel: "Visitors on your website",
                infoType: "Weekly"
            },
            data,
            favoritedCardTitles.includes("Users this week"))
        )

        //Countries
        baseCards.push(
            this.generateCard({
                title: "Users by country",
                type: "doughnut",
                datasetLabel: "",
                infoType: "Countries"
            },
            data,
            favoritedCardTitles.includes("Users by country"))
        )
        
        //Time of day
        baseCards.push(
            this.generateCard({
                title: "Users today",
                type: "line",
                datasetLabel: "Visitors on your website",
                infoType: "Time of day"
            },
            data,
            favoritedCardTitles.includes("Users today"))
        )

        //Time of day percent
        baseCards.push(
            this.generateCard({
                title: "Users by time of day (percent)",
                type: "line",
                datasetLabel: "Visitors on your website",
                infoType: "Time of day percent"
            },
            data,
            favoritedCardTitles.includes("Users by time of day (percent"))
        )

        //Devices
        baseCards.push(
            this.generateCard({
                title: "User devices",
                type: "doughnut",
                datasetLabel: "",
                infoType: "Devices"
            },
            data,
            favoritedCardTitles.includes("User devices"))
        )
        return baseCards
    }

    static generateCard(cardInfo, data, favorited = false) {
        let formattedData = this.formatedDataByType(cardInfo.infoType, data)

        return new LinkCard(
          cardInfo.title, 
          {
            type: cardInfo.type,
            data: {
              labels: this.formatedLabelsByType(cardInfo.infoType, data),
              datasets: [{
                label: cardInfo.datasetLabel,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                data: formattedData,
                borderWidth: 1
              }]
            }
          },
          cardInfo.infoType,
          LinkCard.canDoPredictions(formattedData),
          favorited)
      }
    
    public static formatedLabelsByType(type: string, data: any, withPrediction: boolean = false) {
        switch (type) {
            case "Weekly": 
                var weekdayIndex = new Date().getDay();
                var labels = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thurdsday", "Friday", "Saturday"]
                var formattedLabels = labels.slice(weekdayIndex + 1).concat(labels.slice(0, weekdayIndex + 1))
                return formattedLabels

            case "Countries":
                var countryLabels = []
                for (var item of data) {
                    for(var country of item.countriesInfo) {
                        if (!countryLabels.includes(country.countryCode))
                            countryLabels.push(country.countryCode)
                    }
                }
                if (countryLabels.length === 0)
                    return ["No users tracked"]
                return countryLabels

            case "Time of day":
            case "Time of day percent":
                var hourIndex = Math.floor(new Date().getHours() / 4)
                var labels = ["00:00", "04:00", "08:00", "12:00", "16:00", "20:00"]
                var formattedLabels = labels.slice(hourIndex+1).concat(labels.slice(0, hourIndex + 1))
                if (withPrediction)
                    formattedLabels = formattedLabels.concat(labels.concat("00:00", "04:00").slice(hourIndex + 1, hourIndex + 3))
                return formattedLabels

            case "Devices":
                return ["Desktop", "Mobile", "Tablet"]

            default:
                return ["Sunday", "Monday", "Tuesday", "Wednesday", "Thurdsday", "Friday", "Saturday"]
        }
    }

    private static formatedDataByType(type: string, data: any, withPrediction: boolean = false) {
        //Labels may need to be generated programatically as well (Because of countries)
        switch (type) {
            case "Weekly": 
                var currentDay = new Date()
                var formattedData: number[] = []
                for (var i = 0; i < 7; i++) {
                    formattedData.push(0)

                    for (var item of data) {
                        if (currentDay.getDay() - (6-i) < 0) 
                            var formatDay = currentDay.getDay() + 7 - (6-i) 
                        else 
                            var formatDay = currentDay.getDay() - (6-i)
                            
                        if (new Date(item.timeInterval).getDay() == formatDay) {
                           formattedData[i] += item.clickCount
                        }
                    }
                }
                return formattedData

            case "Countries":
                var formattedData: number[] = []
                var countryLabels = []
                for (var item of data) {
                    for(var country of item.countriesInfo) {
                        if (!countryLabels.includes(country.countryCode)) {
                            countryLabels.push(country.countryCode)
                            formattedData.push(country.clickCount)
                        } else {
                            var cIndex = countryLabels.indexOf(country.countryCode)
                            formattedData[cIndex] += country.clickCount
                        }
                    }
                }
                return formattedData

            case "Time of day":
                var currentDate = new Date()
                currentDate.setHours(Math.floor(currentDate.getHours() / 4) * 4, 0, 0, 0)

                var formattedData: number[] = []
                for (var i = 0; i < 6; i++) {
                    formattedData.push(0)
                    
                    for (var item of data) {
                        var itemInterval = new Date(item.timeInterval)
                        itemInterval.setHours(Math.floor(itemInterval.getHours() / 4) * 4, 0, 0, 0)

                        if ((currentDate.getTime() - itemInterval.getTime()) / (1000*60*60) / 4 == (5 - i)) {
                           formattedData[i] += item.clickCount
                        }
                    }
                }

                return formattedData
            
            case "Time of day percent":
                var currentDate = new Date()
                currentDate.setHours(Math.floor(currentDate.getHours() / 4) * 4, 0, 0, 0)

                var formattedData: number[] = []
                for (var i = 0; i < 6; i++) {
                    formattedData.push(0)
                    
                    for (var item of data) {
                        var itemInterval = new Date(item.timeInterval)
                        itemInterval.setHours(Math.floor(itemInterval.getHours() / 4) * 4, 0, 0, 0)

                        if (((currentDate.getTime() - itemInterval.getTime()) / (1000*60*60) / 4) % 6 == (5 - i)) {
                           formattedData[i] += item.clickCount
                        }
                    }
                }

                var sumOnClicks = formattedData.reduce((a, b) => a + b)
                formattedData = formattedData.map(x => 100 * x / sumOnClicks)
                
                return formattedData

            case "Devices":
                var formattedData: number[] = [0,0,0]
                for(var item of data) {
                    formattedData[0] += item.devicesInfo.desktopCount
                    formattedData[1] += item.devicesInfo.mobileCount
                    formattedData[2] += item.devicesInfo.tabletCount
                }
                return formattedData
                
            default:
                return [20, 5, 10, 25, 45]
        }
    }

    private static canDoPredictions(data: number[]) {
        var nonZeroCount = 0

        for (var item of data)
            if (item !== 0)
                nonZeroCount ++
        
        if (nonZeroCount >= 2)
            return true
        return false
    }

  removePrediction() {
    this.chart.data.datasets.pop();
    this.chart.data.labels = LinkCard.formatedLabelsByType(this.infoType, null, false)
    this.chart.update();
  }

  addPrediction() {
  this.chart.data.datasets.push({
    label: "Prediction",
    fillColor: 'rgba(0,0,0,0)',
    strokeColor: 'rgba(220,180,0,1)',
    pointColor: 'rgba(220,180,0,1)',
    data: this.generatePrediction(this.chart.data.datasets[0].data)
  });

  this.chart.data.labels = LinkCard.formatedLabelsByType(this.infoType, null, true)
  this.chart.update();
  }

  private generatePrediction(data) {
    var extr = new extrapolate()

    for (var index in data) {
      if(data[index] !== 0)
        extr.given(index, data[index])
    }

    var interpolatedData = []
    for (var i = 0; i < data.length; i++) {
      interpolatedData.push(data[i])
    }

    interpolatedData.push(extr.getPoly(data.length + 1))
    interpolatedData.push(extr.getPoly(data.length + 2))
    return interpolatedData
  }
}
