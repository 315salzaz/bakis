//Projektas: Linker4you Front-End
//Vardas Pav: Vilius Kumparskas Povilas Grusas
//Paskirtis: Link class to create and send information to Back-End 
//Code Line:79
import { appInitialize } from '@ionic/angular/app-initialize';
import { LinkCard } from 'src/app/Classes/link-card'
import { TaskService } from '../task.service';

export class Link {
    linkName: string;
    linkCards: LinkCard[];
    isInDashboard: boolean;
    isCollapsed: boolean;
    shortUrl: string;
    shortfullUrl: string;
    weekIntervals: any;

    private minVisibleCardCount: Number = 2;
    private visibleCardCount: Number = this.minVisibleCardCount;

    constructor(linkName: string, isInDashboard: boolean = false, shortUrl: string) {
        this.linkName = linkName
        this.isInDashboard = isInDashboard
        this.isCollapsed = true
        this.shortUrl = shortUrl
        this.shortfullUrl = "http://localhost:8100/l4u/" + shortUrl
    }

    async getLinkClickData(taskService: TaskService) {
        await new Promise((resolve, _) =>
            taskService.getWeekClickIntervalsOfLink(this.shortUrl).subscribe((response:any[]) => {
                resolve(response)
             })
        ).then((response) => {
            this.weekIntervals = response
            this.weekIntervals.sort((a, b) => {
                if(Date.parse(a.timeInterval) > Date.parse(b.timeInterval))
                    return 1
                if(Date.parse(a.timeInterval) < Date.parse(b.timeInterval))
                    return -1
                return 0
            })
        })
    }

    generateBaseCards(favoritedCardTitles: string[]) {
        this.linkCards = LinkCard.generateBaseCards(favoritedCardTitles, this.weekIntervals)
    }

    async generateFavoriteCards(taskService: TaskService, data: any) {
        var cards: LinkCard[] = []
    
        await new Promise((resolve, _) =>
          taskService.getLinkCardInfos(this.shortUrl).subscribe((response:any[]) => {
            resolve(response)
          })
        ).then(cardInfos => {
          for (var cardInfo of cardInfos as any[]) {
            var card = LinkCard.generateCard(cardInfo, data)
            cards.push(card)
          }
        })
    
        this.linkCards = cards
      }

    addLinkCard(card: LinkCard) {
        this.linkCards.push(card)
    }

    getVisibleCardCount(): Number {
        return this.visibleCardCount
    }

    getMinVisibleCardCount(): Number {
        return this.minVisibleCardCount
    }
        
    changeVisibleCardCount() {
        if (this.visibleCardCount > this.minVisibleCardCount)
            this.visibleCardCount = this.minVisibleCardCount
        else
            this.visibleCardCount = this.linkCards.length
    }
}
