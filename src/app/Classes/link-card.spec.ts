import { LinkCard } from './link-card';

describe('LinkCard', () => {
  
  it('getDefaultFavoriteCardInfo', () => {
    let def = LinkCard.getDefaultFavoriteCardInfo("shorturl")
    expect(def.title).toBe("Users this week")
  })

  it('generateBaseCards', () => {
    let cards = LinkCard.generateBaseCards(
      [], 
      [{
      timeInterval: "2021-03-25T12:00:00Z",
      clickCount: 10,
      countriesInfo: [{countryCode: "LT", clickCount: 10}],
      devicesInfo: {tabletCount: 10, mobileCount: 0, desktopCount: 0}
    }])
  
  expect(cards.length).toBe(5)
  })

  it('formatedLabelsByType-Devices', () => {
    let labels = LinkCard.formatedLabelsByType("Devices", [{
      timeInterval: "2021-03-25T12:00:00Z",
      clickCount: 10,
      countriesInfo: [{countryCode: "LT", clickCount: 10}],
      devicesInfo: {tabletCount: 10, mobileCount: 0, desktopCount: 0}
    }])
  
  expect(labels.length).toBe(3)
  })

  it('formatedLabelsByType-Time of day', () => {
    let labels = LinkCard.formatedLabelsByType("Time of day", [{
      timeInterval: "2021-03-25T12:00:00Z",
      clickCount: 10,
      countriesInfo: [{countryCode: "LT", clickCount: 10}],
      devicesInfo: {tabletCount: 10, mobileCount: 0, desktopCount: 0}
    }])
  
  expect(labels.length).toBe(6)
  })

  it('formatedLabelsByType-Countries', () => {
    let labels = LinkCard.formatedLabelsByType("Countries", [{
      timeInterval: "2021-03-25T12:00:00Z",
      clickCount: 10,
      countriesInfo: [{countryCode: "LT", clickCount: 10}],
      devicesInfo: {tabletCount: 10, mobileCount: 0, desktopCount: 0}
    }])
  
  expect(labels[0]).toBe("LT")
  })

  it('formatedLabelsByType-Weekly', () => {
    let labels = LinkCard.formatedLabelsByType("Weekly", [{
      timeInterval: "2021-03-25T12:00:00Z",
      clickCount: 10,
      countriesInfo: [{countryCode: "LT", clickCount: 10}],
      devicesInfo: {tabletCount: 10, mobileCount: 0, desktopCount: 0}
    }])
  
  expect(labels.length).toBe(7)
  })

  it('generateCard-Weekly', () => {
    let newCard = LinkCard.generateCard({
      title: "Weekly",
      type: "doughnut",
      datasetLabel: "",
      infoType: "Weekly"
    },
    [{
      timeInterval: "2021-03-25T12:00:00Z",
      clickCount: 10,
      countriesInfo: [{countryCode: "LT", clickCount: 10}],
      devicesInfo: {tabletCount: 10, mobileCount: 0, desktopCount: 0}
    }]
  )
  
  expect(newCard.chartInfo.data.labels.length).toBe(7)
  expect(newCard.chartInfo.data.datasets[0].data).toEqual([0, 10, 0, 0, 0, 0, 0])
  })

  it('generateCard-Countries', () => {
    let newCard = LinkCard.generateCard({
        title: "Countries",
        type: "doughnut",
        datasetLabel: "",
        infoType: "Countries"
      },
      [{
        timeInterval: "2015-03-25T12:00:00Z",
        clickCount: 10,
        countriesInfo: [{countryCode: "LT", clickCount: 10}],
        devicesInfo: {tabletCount: 10, mobileCount: 0, desktopCount: 0}
      }]
    )
    
    expect(newCard.chartInfo.data.labels[0]).toEqual('LT')
    expect(newCard.chartInfo.data.datasets[0].data).toEqual([10])
  });

  it('generateCard-UserDevices', () => {
    let newCard = LinkCard.generateCard({
        title: "User devices",
        type: "doughnut",
        datasetLabel: "",
        infoType: "Devices"
      },
      [{
        timeInterval: "2015-03-25T12:00:00Z",
        clickCount: 10,
        countriesInfo: [{countryCode: "LT", clickCount: 10}],
        devicesInfo: {tabletCount: 10, mobileCount: 0, desktopCount: 0}
      }]
    )
    
    expect(newCard.chartInfo.data.labels.length).toBe(3)
    expect(newCard.chartInfo.data.datasets[0].data).toEqual([0, 0, 10])
  });

  it('generateCard-UserDevices', () => {
    let newCard = LinkCard.generateCard({
        title: "User devices",
        type: "doughnut",
        datasetLabel: "",
        infoType: "Devices"
      },
      [{
        timeInterval: "2015-03-25T12:00:00Z",
        clickCount: 10,
        countriesInfo: [{countryCode: "LT", clickCount: 10}],
        devicesInfo: [{tabletCount: 0, mobileCount: 0, desktopCount: 0}]
      }]
    )
    
    expect(newCard.chartInfo.data.labels.length).toBe(3)
  });

  it('should create an instance', () => {
    expect(new LinkCard(
      "Card text",
      {
        type: "Weekly",
        data: {
          labels: LinkCard.formatedLabelsByType(
            "Weekly", 
            [{
              timeInterval: "2015-03-25T12:00:00Z",
              clickCount: 10,
              countriesInfo: [{countryCode: "LT", clickCount: 10}],
              devicesInfo: [{tabletCount: 0, mobileCount: 0, desktopCount: 0}]
            }]
          ),
          datasets: [{
            label: "dsLabel",
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            data: 10,
            borderWidth: 1
          }]
        }
      },
      "Weekly",
      false
    )).toBeTruthy();
  });
});
