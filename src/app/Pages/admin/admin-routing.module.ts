import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminPage } from './admin.page';

const routes: Routes = [
  {
    path: 'navigation-bar',
    component: AdminPage,
    children: [
      {
        path: 'users-list',
        loadChildren: () => import("src/app/Pages/users-list/users-list.module").then(m => m.UsersListPageModule)
      },
      {
        path: 'account',
        loadChildren: () => import("src/app/Pages/account/account.module").then(m => m.AccountPageModule)
      }
    ]
  },
  {
    path: '',
    redirectTo: 'navigation-bar/users-list',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminPageRoutingModule {}
