//Projektas: Linker4you Front-End
//Vardas Pav: Vilius Kumparskas Povilas Grusas
//Paskirtis: Dashboard screen with PDF generator API plus Favorite links
//Code Line:63
import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Link } from 'src/app/Classes/link';
import { LinkCard } from 'src/app/Classes/link-card';
import { TaskService } from 'src/app/task.service';
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs =pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  links: Link[] = [];
  pdfOjb = null;
  constructor(private taskService: TaskService) { }

  ngOnInit(){}
  
  ionViewWillEnter(){
    this.setupLinks();
  }

  setupLinks() {
    new Promise((resolve, _) =>
      this.taskService.getAllLinks().subscribe((response:any[]) => {
        resolve(response)
      })
    ).then(async linkInfos => {
      this.links = [];
      for (var linkInfo of linkInfos as any[]) {
        var currentLink = new Link(linkInfo.title, true, linkInfo.shortUrl)

        await currentLink.getLinkClickData(this.taskService)
        await currentLink.generateFavoriteCards(this.taskService, currentLink.weekIntervals)
        this.links.push(currentLink)
      }
    })
  }
}
