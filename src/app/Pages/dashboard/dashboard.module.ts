import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DashboardPageRoutingModule } from './dashboard-routing.module';
import { DashboardPage } from './dashboard.page';
import { LinkTableComponent } from 'src/app/Components/link-table/link-table.component'
import { LinkCardComponent } from 'src/app/Components/link-table/link-card/link-card.component';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { QrCodeComponent } from 'src/app/Components/qr-code/qr-code.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DashboardPageRoutingModule,
    NgxQRCodeModule
  ],
  declarations: [
    DashboardPage,
    LinkTableComponent,
    LinkCardComponent,
    QrCodeComponent
  ]
})
export class DashboardPageModule {}
