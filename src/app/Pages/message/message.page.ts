import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Socket } from 'ngx-socket-io';
import { AuthService } from 'src/app/auth.service';
import { TaskService } from 'src/app/task.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.page.html',
  styleUrls: ['./message.page.scss'],
})
export class MessagePage implements OnInit {
  message = '';
  messages =[];
  User = '';
  constructor(private socket: Socket, private toastCtrl: ToastController) { }

  ngOnInit() {

  this.socket.connect();
  
  let name = `User-${new Date().getTime()}`;
  this.User = name;

  this.socket.emit('set-name', name);

  this.socket.fromEvent('users-changed').subscribe(data =>{
   
    let user = data['user'];
    if(data['event'] == 'left') {
      this.showToast(`User left the chat: ${user}`);
    } else {
      this.showToast(`User joined the chat: ${user}`);
    }
  });
  this.socket.fromEvent('message').subscribe(message => {
    console.log('New', message);
    this.messages.push(message);
  })
  }
  
  sendMessage() {
    this.socket.emit('send-message', {text: this.message});
    this.message ='';
  }
  ionViewWIllLeave() {
    this.socket.disconnect()
  }
  async showToast(msg){
    let toast = await this.toastCtrl.create({
      message: msg,
      position: 'middle',
      duration: 3000
    });
    toast.present();
  }

}
