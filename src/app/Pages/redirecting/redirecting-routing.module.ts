import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RedirectingPage } from './redirecting.page';

const routes: Routes = [
  {
    path: '',
    //Need error component -> redirect -> log-in-page
    component: RedirectingPage
  },
  {
    path: '**',
    component: RedirectingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RedirectingPageRoutingModule {}
