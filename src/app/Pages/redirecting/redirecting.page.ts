import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-redirecting',
  templateUrl: './redirecting.page.html',
  styleUrls: ['./redirecting.page.scss'],
})
export class RedirectingPage implements OnInit {
  currentShortLink: string = ""
  private countryData: string = ""
  constructor(private router: Router) { }

  ngOnInit() {
    let urlParts = this.router.url.split("/")
    this.currentShortLink = urlParts[urlParts.length - 1]

    var deviceType = this.getDeviceInfo()
    this.getCountryData().then(()=> {
      this.doRedirect(deviceType, this.countryData)
    })
  }

  doRedirect(deviceType, countryData) {
    window.location.href = `http://localhost:3000/r/${this.currentShortLink}/${deviceType}/${countryData}`
  }

  async getCountryData() {
    await fetch('https://ipinfo.io/json?token=898d6c079476db')
    .then(res => res.json()).then(
      data => {this.countryData = data.country}
    )
  }

  getDeviceInfo() {
    const ua = navigator.userAgent;
    if (/(tablet|ipad|playbook|silk)|(android(?!.*mobi))/i.test(ua)) {
      return "tablet";
    }
    if (
      /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Kindle|Silk-Accelerated|(hpw|web)OS|Opera M(obi|ini)/.test(
        ua
      )
    ) {
      return "mobile";
    }
    return "desktop";
  }
}
