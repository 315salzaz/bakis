import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RedirectingPageRoutingModule } from './redirecting-routing.module';

import { RedirectingPage } from './redirecting.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RedirectingPageRoutingModule
  ],
  declarations: [RedirectingPage]
})
export class RedirectingPageModule {}
