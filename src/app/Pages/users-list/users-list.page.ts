import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth.service';
import { TaskService } from 'src/app/task.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.page.html',
  styleUrls: ['./users-list.page.scss'],
})
export class UsersListPage implements OnInit {
  userIds: any[] = []
  userEmails: any[] = []
  constructor(private task: TaskService, private authService: AuthService) {
    
  }

  private setUsersList() {
    this.task.getUsersList(this.authService.getUserId()).subscribe(resp => {      
      this.userIds = (resp as any[]).map(user => user._id)
      this.userEmails = (resp as any[]).map(user => user.email)
    })
  }

  ngOnInit() {
    this.setUsersList()
  }

}
