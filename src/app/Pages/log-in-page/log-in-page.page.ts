//Projektas: Linker4you Front-End
//Vardas Pav: Vilius Kumparskas Povilas Grusas
//Paskirtis: Log-in screen with autentification
//Code Line:25
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth.service';
import { HttpResponse } from '@angular/common/http';
import { AccountPage } from '../account/account.page';
import { NotLogInComponent } from 'src/app/Components/not-log-in/not-log-in.component';
import { PopoverController } from '@ionic/angular'
import { WebReqInterceptor } from 'src/app/web-req.interceptor';

@Component({
  selector: 'app-log-in-page',
  templateUrl: './log-in-page.page.html',
  styleUrls: ['./log-in-page.page.scss'],
})
export class LogInPagePage implements OnInit {

  public login
  constructor(public popoverloginController: PopoverController, private authService: AuthService, private router: Router, private WebReqInterceptor: WebReqInterceptor) { }

  ngOnInit() {
  }
  
  onLoginButtonClicked(email: string, password: string) {
    this.authService.login(email, password).subscribe((res: HttpResponse<any>) => {
      this.login = this.WebReqInterceptor.login
      if (res.status === 200) {
        // we have logged in successfully
        if (!res.body.isAdmin)
          this.router.navigate(['/home/navigation-bar/links'])
        else 
          this.router.navigate(['/admin'])
        this.WebReqInterceptor.login === true
      }
      else if(res.status ===400){
      
      }
       
      
      console.log(res);
      
    });
  }
  async includeloginpopover(ev: any) {
    const popover = await this.popoverloginController.create({
      component: NotLogInComponent,
      cssClass: 'my-custom-class',
      event: ev,
      translucent: true
    });
    return await popover.present();
  }
}
