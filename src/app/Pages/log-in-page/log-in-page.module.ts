import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LogInPagePageRoutingModule } from './log-in-page-routing.module';

import { LogInPagePage } from './log-in-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LogInPagePageRoutingModule
  ],
  declarations: [LogInPagePage]
})
export class LogInPagePageModule {}
