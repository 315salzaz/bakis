import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LogInPagePage } from './log-in-page.page';

const routes: Routes = [
  {
    path: '',
    component: LogInPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LogInPagePageRoutingModule {}
