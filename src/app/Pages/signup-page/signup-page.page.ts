//Projektas: Linker4you Front-End
//Vardas Pav: Vilius Kumparskas Povilas Grusas
//Paskirtis: Registering on the system
//Code Line:25
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth.service';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-signup-page',
  templateUrl: './signup-page.page.html',
  styleUrls: ['./signup-page.page.scss'],
})
export class SignupPagePage implements OnInit {

  constructor(private authService: AuthService,private router: Router) { }

  ngOnInit() {
  }
  onSignupButtonCLicked(email: string, password: string) {
    this.authService.signup(email, password).subscribe((res: HttpResponse<any>) => {
      if (res.status === 200) {
        // we have register successfully
        this.router.navigate(['/log-in-page']);
      }
      console.log(res);
      
    });
  }
}
