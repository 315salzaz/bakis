import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth.service';
import { CreateNoteComponent } from 'src/app/Components/create-note/create-note.component';
import { TaskService } from 'src/app/task.service';
import { PopoverController } from '@ionic/angular'
import { EditNoteComponent } from 'src/app/Components/edit-note/edit-note.component';
import { DeleteNoteComponent } from 'src/app/Components/delete-note/delete-note.component';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {
  constructor(public popoverController: PopoverController,private authservice: AuthService,private taskservice: TaskService) { }
  returnuser : any;
  notes: any[];

  ngOnInit() {
   this.taskservice.getuserinfo(this.authservice.getUserId()).subscribe(res => {
    this.returnuser = res})
   this.taskservice.getNote().subscribe((notes:any[])=>{
    this.notes=notes;
  })
  }

  private getCurrentTheme() {
    if (1 < document.documentElement.classList.value.indexOf("light")) {
      return "light"
    } else if (1 < document.documentElement.classList.value.indexOf("colorless")) {
      return "colorless"
    } else {
      return ""
    }
  }

  private getNextTheme(current: string) {
    if (current == "colorless") {
      return "light"
    } else if (current == "light") {
      return ""
    } else {
      return "colorless"
    }
  }

  protected switchTheme() {
    let currentTheme = this.getCurrentTheme()
    if (currentTheme != "")
      document.documentElement.classList.remove(currentTheme)

    let nextTheme = this.getNextTheme(currentTheme)
    if(nextTheme != "")
      document.documentElement.classList.add(nextTheme)
  }

  async includenotepopover(ev: any) {
    const popover = await this.popoverController.create({
      component: CreateNoteComponent,
      cssClass: 'my-custom-class',
      event: ev,
      translucent: true
    });
    return await popover.present();
  }

  async includeeditnotepopover(ev: any, id : string) {
    const popover = await this.popoverController.create({
      component: EditNoteComponent,
      cssClass: 'my-custom-classs',
      event: ev,
      translucent: true,
      componentProps: {id: id}
    });
    return await popover.present();
  }

  async includedeletenotepopover(ev: any, id : string) {
    const popover = await this.popoverController.create({
      component: DeleteNoteComponent,
      cssClass: 'my-custom-classs',
      event: ev,
      translucent: true,
      componentProps: {id: id}
    });
    return await popover.present();
  }

}
