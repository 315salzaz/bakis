import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LinksPageRoutingModule } from './links-routing.module';
import { LinksPage } from './links.page';
import { LinkTableComponent } from 'src/app/Components/link-table/link-table.component'
import { LinkCardComponent } from 'src/app/Components/link-table/link-card/link-card.component';
import { QrCodeComponent } from 'src/app/Components/qr-code/qr-code.component';
import { NgxQRCodeModule } from 'ngx-qrcode2';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LinksPageRoutingModule,
    NgxQRCodeModule
  ],
  declarations: [
    LinksPage, 
    LinkTableComponent,
    LinkCardComponent,
    QrCodeComponent
  ]
})
export class LinksPageModule {}
