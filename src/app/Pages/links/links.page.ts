//Projektas: Linker4you Front-End
//Vardas Pav: Vilius Kumparskas Povilas Grusas
//Paskirtis: Setting up links
//Code Line:83
import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular'
import { CreateLinkModalComponent } from 'src/app/Components/create-link-modal/create-link-modal.component'
import { Link } from "src/app/Classes/link"
import { TaskService } from 'src/app/task.service';
import { AuthService } from 'src/app/auth.service';
import { NotLogInComponent } from 'src/app/Components/not-log-in/not-log-in.component';

@Component({
  selector: 'app-links',
  templateUrl: './links.page.html',
  styleUrls: ['./links.page.scss'],
})
export class LinksPage implements OnInit {
  links: Link[] = [];
  constructor(public popoverController: PopoverController, private taskService: TaskService, private autentification: AuthService) { }

  ngOnInit() {}

  ionViewWillEnter(){
    this.setupLinks();
  }

  DeleteShortUrl(shorturl:string){
    this.taskService.deleteUrlinfo(shorturl).subscribe((res:any) =>{
      console.log(res)
    })
  }

  async presentPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: CreateLinkModalComponent,
      cssClass: 'my-custom-class',
      event: ev,
      translucent: true
    });
    return await popover.present();
  }
  async includeloginpopover(ev: any) {
    const popover = await this.popoverController.create({
      component: NotLogInComponent,
      cssClass: 'my-custom-class',
      event: ev,
      translucent: true
    });
    return await popover.present();
  }
  setupLinks() {
    new Promise((resolve, _) =>
      this.taskService.getAllLinks().subscribe((response:any[]) => {
        resolve(response)
        console.log(response)
      })
    ).then(async (linkInfos) => {
      this.links = []
      for (var linkInfo of linkInfos as any[]) {
        var currentLink = new Link(linkInfo.title, false, linkInfo.shortUrl)

        await currentLink.getLinkClickData(this.taskService)
        var favoritedCardTitles = await this.getFavoritedCardTitles(linkInfo.shortUrl)
        currentLink.generateBaseCards(favoritedCardTitles)

        this.links.push(currentLink)
      }
    })
  }

  async getFavoritedCardTitles(shortUrl: string): Promise<string[]> {
    var cardTitles: string[] = []

    await new Promise((resolve, _) =>
      this.taskService.getLinkCardInfos(shortUrl).subscribe((response:any[]) => {
        resolve(response)
      })
    ).then(cardInfos => {
      for (var cardInfo of cardInfos as any[]) {
        cardTitles.push(cardInfo.title)
      }
    })

    return cardTitles
  }
}
