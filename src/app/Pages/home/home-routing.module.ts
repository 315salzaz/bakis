//Projektas: Linker4you Front-End
//Vardas Pav: Vilius Kumparskas Povilas Grusas
//Paskirtis: Home screen navigation and routing
//Code Line:30
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePage } from './home.page';

const routes: Routes = [
  {
    path: 'navigation-bar',
    component: HomePage,
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import("src/app/Pages/dashboard/dashboard.module").then(m => m.DashboardPageModule)
      },
      {
        path: 'links',
        loadChildren: () => import("src/app/Pages/links/links.module").then(m => m.LinksPageModule)
      },
      {
        path: 'account',
        loadChildren: () => import("src/app/Pages/account/account.module").then(m => m.AccountPageModule)
      },
      {
        path: 'message',
        loadChildren: () => import('src/app/Pages/message/message.module').then( m => m.MessagePageModule)
      }
      
    ]
  },
  {
    path: '',
    redirectTo: 'navigation-bar/links',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
  
})
export class HomePageRoutingModule {}
