import { Component, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { TaskService } from 'src/app/task.service';

@Component({
  selector: 'app-edit-link',
  templateUrl: './edit-link.component.html',
  styleUrls: ['./edit-link.component.scss'],
})
export class EditLinkComponent implements OnInit {

  constructor(private taskservice: TaskService, private navparams: NavParams) { }

  ngOnInit() {}
  protected editlink(title:string) 
  {
  this.taskservice.updatelinktitle(title,this.navparams.get("shortUrl")).subscribe(res =>{
    console.log(res)
  }) 
  }
}
