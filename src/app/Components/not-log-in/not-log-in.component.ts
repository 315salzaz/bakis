import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth.service';

@Component({
  selector: 'app-not-log-in',
  templateUrl: './not-log-in.component.html',
  styleUrls: ['./not-log-in.component.scss'],
})
export class NotLogInComponent implements OnInit {

  constructor(private autentification: AuthService,private router: Router) { }

  ngOnInit() {}
  Logout(){
    this.autentification.logout()
  }
  Stay(){
    this.router.navigate(['/home/navigation-bar/links'])
  }
}
