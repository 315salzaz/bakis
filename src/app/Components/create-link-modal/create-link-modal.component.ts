//Projektas: Linker4you Front-End
//Vardas Pav: Vilius Kumparskas Povilas Grusas
//Paskirtis: Creating short link either custom ending or auto-generating
//Code Line:35
import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular'
import { LinkCard } from 'src/app/Classes/link-card';
import { TaskService } from 'src/app/task.service';

@Component({
  selector: 'app-create-link-modal',
  templateUrl: './create-link-modal.component.html',
  styleUrls: ['./create-link-modal.component.scss'],
})
export class CreateLinkModalComponent implements OnInit {

  constructor(public popoverController: PopoverController, private taskService:TaskService) { }

  ngOnInit() {}

  createShortUrl(full: string, title: string) {
    this.taskService.createShortLink(title, full).subscribe((response:any)=> {
      this.taskService.createLinkCard(LinkCard.getDefaultFavoriteCardInfo(response.shortUrl)).subscribe()
      console.log(response)
    });
  }

  createShortUrlCustom(full: string, title: string, shortUrl: string) {
    this.taskService.createShortLinkCustom(title, full, shortUrl).subscribe((response:any)=> {
      this.taskService.createLinkCard(LinkCard.getDefaultFavoriteCardInfo(response.shortUrl)).subscribe()
      console.log(response)
    });
  }

  createlink(full: string, title: string, shortUrl: string) {
    if(shortUrl === "")
      this.createShortUrl(full, title);
    else
      this.createShortUrlCustom(full, title, shortUrl)
  }
  
}
