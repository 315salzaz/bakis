//Projektas: Linker4you Front-End
//Vardas Pav: Vilius Kumparskas Povilas Grusas
//Paskirtis: QR API implementing
//Code Line: 15
import { Component, Input, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-qr-code',
  templateUrl: './qr-code.component.html',
  styleUrls: ['./qr-code.component.scss'],
})
export class QrCodeComponent implements OnInit {
  @Input() qrData: string

  constructor() { }

  ngOnInit() {}
  
  ngOnChanges(changes: SimpleChanges) {
    this.qrData = "http://localhost:3000/r/" + changes.qrData.currentValue
  }

}
