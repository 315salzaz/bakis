import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavParams } from '@ionic/angular';
import { TaskService } from 'src/app/task.service';

@Component({
  selector: 'app-delete-note',
  templateUrl: './delete-note.component.html',
  styleUrls: ['./delete-note.component.scss'],
})
export class DeleteNoteComponent implements OnInit {

  constructor(private taskservice: TaskService, private navparams: NavParams, public router: Router) { }

  ngOnInit() {}

  Confirm(){

    this.taskservice.deleteNote(this.navparams.get("id")).subscribe(res =>{
      console.log(res)
  })
}
}
