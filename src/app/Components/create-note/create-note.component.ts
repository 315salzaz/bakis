import { Component, OnInit } from '@angular/core';
import { TaskService } from 'src/app/task.service';

@Component({
  selector: 'app-create-note',
  templateUrl: './create-note.component.html',
  styleUrls: ['./create-note.component.scss'],
})
export class CreateNoteComponent implements OnInit {

  constructor(private taskService: TaskService) { }

  ngOnInit() {}
  createNote(title: string, description: string) {
    this.taskService.createnote(title,description).subscribe();
  }
}
