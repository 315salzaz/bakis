import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.scss'],
})
export class NavigationBarComponent implements OnInit {

  constructor(private router: Router) {}

  ngOnInit() {
    let pageParts = this.router.url.split("/")
    if (pageParts.includes("admin"))
      document.getElementById("home-nav-bar").remove()
    else 
      document.getElementById("admin-nav-bar").remove()
  }

}
