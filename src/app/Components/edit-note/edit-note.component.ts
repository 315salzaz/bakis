import { Component, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { TaskService } from 'src/app/task.service';

@Component({
  selector: 'app-edit-note',
  templateUrl: './edit-note.component.html',
  styleUrls: ['./edit-note.component.scss'],
})
export class EditNoteComponent implements OnInit {

  constructor(protected taskservice: TaskService, private navparams: NavParams) { }

  ngOnInit() {}
  protected editnote(description:string) 
  {
  this.taskservice.updateNote(this.navparams.get("id"),description).subscribe(res =>{
    console.log(res)
  })
}
}
