//Projektas: Linker4you Front-End
//Vardas Pav: Vilius Kumparskas Povilas Grusas
//Paskirtis: Link information putting into tables
//Code Line:60
import { Component, ElementRef, Input, OnInit, ViewChild, Directive } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Link } from "src/app/Classes/link"
import { LinkCard } from 'src/app/Classes/link-card';
import { TaskService } from 'src/app/task.service';
import { EditLinkComponent } from '../edit-link/edit-link.component';
import { PopoverController } from '@ionic/angular'
import { componentFactoryName } from '@angular/compiler';
import { NgxQrcodeElementTypes, NgxQrcodeErrorCorrectionLevels} from '@techiediaries/ngx-qrcode'
import { DeleteLinkComponentComponent } from '../delete-link-component/delete-link-component.component';

@Component({
  selector: 'app-link-table',
  templateUrl: './link-table.component.html',
  styleUrls: ['./link-table.component.scss'],
})
export class LinkTableComponent implements OnInit {
  @Input() link: Link;
  
  protected moreButtonText: string = "Show more"
  public elemtTypes = NgxQrcodeElementTypes.URL
  public NgxQrcodeErrorCorrectionLevels = NgxQrcodeErrorCorrectionLevels.HIGH

  constructor(public popoverController: PopoverController, private taskService: TaskService) {}

  ngOnInit() {}

  protected copyToClipboard() {
    navigator.clipboard.writeText(this.link.shortUrl)
  }

  protected linkToggled() {
    this.link.isCollapsed = !this.link.isCollapsed
  }

  protected updateTitle(){

  }
  protected moreButtonClicked() {
    this.link.changeVisibleCardCount()

    if(this.moreButtonText == "Show more")
      this.moreButtonText = "Show less"
    else
      this.moreButtonText = "Show more"
  }

  protected shouldShowMoreButton(): boolean {
    return this.link.linkCards.length > this.link.getMinVisibleCardCount()
  }
  public downloadQRCode() {
    const fileNameToDownload = 'image_qrcode';
    const base64Img = document.getElementsByClassName('coolQRCode')[0].children[0]['src'];
    fetch(base64Img)
       .then(res => res.blob())
       .then((blob) => {
          // IE
          if (window.navigator && window.navigator.msSaveOrOpenBlob){
             window.navigator.msSaveOrOpenBlob(blob,fileNameToDownload);
          } else { // Chrome
             const url = window.URL.createObjectURL(blob);
             const link = document.createElement('a');
             link.href = url;
             link.download = fileNameToDownload;
             link.click();
          }
       })
 }
  protected favoriteRemoveClicked(card: LinkCard, shortUrl: string) {
    this.taskService.deleteLinkCard(shortUrl, card.cardText).subscribe()
    this.link.linkCards.splice(this.link.linkCards.indexOf(card), 1)
  }

  protected predictionClicked(card: LinkCard) {
    card.isPredicting = !card.isPredicting
    if(card.isPredicting)
      card.addPrediction()
    else
      card.removePrediction()
  }

  protected favoritePressed(card: LinkCard, shortUrl: string) {
    var cardInfo: any = {
      title: card.cardText,
      type: card.chartInfo.type,
      labels: card.chartInfo.data.labels,
      datasetLabel: card.chartInfo.data.datasets[0].label,
      infoType: card.infoType,
      shortUrl: shortUrl
    }

    card.favorited = !card.favorited

    if(card.favorited)
      this.taskService.createLinkCard(cardInfo).subscribe()
    else
      this.taskService.deleteLinkCard(shortUrl, card.cardText).subscribe()
  }
  async presentPopoverEdit(ev: any) {
    const popover = await this.popoverController.create({
      component: EditLinkComponent,
      cssClass: 'my-custom-class',
      event: ev,
      translucent: true,
      componentProps: {shortUrl: this.link.shortUrl}
    });
    return await popover.present();
  }
  async presentPopoverDelete(ev: any) {
    const popover = await this.popoverController.create({
      component: DeleteLinkComponentComponent,
      cssClass: 'my-custom-class',
      event: ev,
      translucent: true,
      componentProps: {shortUrl: this.link.shortUrl}
    });
    return await popover.present();
  }

}
