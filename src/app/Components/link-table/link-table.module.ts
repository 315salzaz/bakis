import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { NgxQRCodeModule } from 'ngx-qrcode2'

import { LinkTableComponent } from './link-table.component';
import { LinkCardComponent } from './link-card/link-card.component';
import { QrCodeComponent } from '../qr-code/qr-code.component'

@NgModule({
  declarations: [
    LinkCardComponent,
    LinkTableComponent,
    QrCodeComponent
  ],
  imports: [
    CommonModule,
    NgxQRCodeModule,
    IonicModule
  ]
})
export class LinkTableModule { }
