//Projektas: Linker4you Front-End
//Vardas Pav: Vilius Kumparskas Povilas Grusas
//Paskirtis: Link one Card Information
//Code Line:30
import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { LinkCard } from 'src/app/Classes/link-card';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-link-card',
  templateUrl: './link-card.component.html',
  styleUrls: ['./link-card.component.scss'],
})
export class LinkCardComponent implements OnInit, AfterViewInit {
  @Input() linkCard: LinkCard;
  @ViewChild('linkCardCanvas') canvas: ElementRef;

  constructor() { }
  ngAfterViewInit(): void {
    this.createChart()
  }

  ngOnInit() {
    
  }

  createChart() {
    var ctx = (this.canvas).nativeElement.getContext('2d');
    this.linkCard.chart = new Chart(ctx, this.linkCard.chartInfo)
  }
}
