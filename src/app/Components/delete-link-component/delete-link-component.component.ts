import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavParams } from '@ionic/angular';
import { TaskService } from 'src/app/task.service';

@Component({
  selector: 'app-delete-link-component',
  templateUrl: './delete-link-component.component.html',
  styleUrls: ['./delete-link-component.component.scss'],
})
export class DeleteLinkComponentComponent implements OnInit {

  constructor(private taskservice: TaskService, private navparams: NavParams, public router: Router) { }

  ngOnInit() {}
  Confirm(){

    this.taskservice.deletelinks(this.navparams.get("shortUrl")).subscribe(res =>{
      console.log(res)
  })

  }
  Stay(){
    this.router.navigate(['/home/navigation-bar/links'])
  }
}
