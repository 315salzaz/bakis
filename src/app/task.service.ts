//Projektas: Linker4you Front-End
//Vardas Pav: Vilius Kumparskas Povilas Grusas
//Paskirtis: Web_request to Back_End roots and functions
//Code Line:45
import { i18nMetaToJSDoc } from '@angular/compiler/src/render3/view/i18n/meta';
import { Injectable } from '@angular/core';
import { WebRequestService } from './web-request.service';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(private WebRequestService:WebRequestService) { }

  getWeekClickIntervalsOfLink(shortUrl: string) {
    return this.WebRequestService.get(`ClickInterval_Get_WeekOfLink/${shortUrl}`)
  }

  createLinkCard(linkCardInfo: any) {
    return this.WebRequestService.post(`LinkCard_Post_LinkCardInfo`, linkCardInfo);
  }

  getLinkCardInfos(shortUrl: string) {
    return this.WebRequestService.get(`LinkCard_Get_LinkCardInfos/${shortUrl}`);
  }

  deleteLinkCard(shortUrl:string, title: string) {
    return this.WebRequestService.delete(`LinkCard_Delete_DeleteGraphByTitle/${shortUrl}/${title}`);
  }

  createShortLinkCustom(title: string, fullUrl: string, shortUrl: string) {
    return this.WebRequestService.post(`Link_Post_NewLink`, {title, fullUrl, shortUrl})
  }
  createShortLink(title: string, fullUrl: string) {
    return this.WebRequestService.post(`Link_Post_NewLink`, {title, fullUrl})
  }

  getAllLinks() {
    return this.WebRequestService.get(`Link_Get_All`)
  }

  createshortUrl(full: string) {
    //we want to get shortURl
    return this.WebRequestService.post(`home/navigation-bar/links`,{full});
  }
  getUrlinfo() {
    return this.WebRequestService.get(`home/navigation-bar/links`);
  }
  doRedirect(shortUrl:string, userIP: any, deviceInfo: any) {
    return this.WebRequestService.get(`r/${shortUrl}/${deviceInfo}`);
  }
  deleteUrlinfo(shorturl:string) {
    return this.WebRequestService.delete(`home/navigation-bar/links/${shorturl}`);
  }
  getuseremail(){
    return this.WebRequestService.get(`account`);
  }
  getuserinfo(id: string){
    return this.WebRequestService.get(`home/navigation-bar/links/account/${id}`);
  }
  getuserinfoformessages(id: string){
    return this.WebRequestService.get(`home/navigation-bar/links/message/${id}`);
  }
  updatelinktitle(title: string, shortURl:string)
  {
    return this.WebRequestService.patch(`Link_Get_All/${shortURl}`,{title});
  }
  deletelinks(shortURl: string)
  {
    return this.WebRequestService.delete(`Link_Delete_WipeAll/${shortURl}`)
  }
  createnote(title: string, description: string)
  {
    return this.WebRequestService.post(`home/navigation-bar/links/account/notes`,{title,description})
  }
  getNote()
  {
    return this.WebRequestService.get(`home/navigation-bar/links/account/notes`)
  }
  deleteNote(id : string)
  {
    return this.WebRequestService.delete(`home/navigation-bar/links/account/notes/${id}`)
  }
  updateNote(id: string, description: string)
  {
    return this.WebRequestService.patch(`home/navigation-bar/links/account/notes/${id}`,{description});
  }
  getUsersList(id: string)
  {
    return this.WebRequestService.get(`admin/accounts/${id}`)
  }
}
