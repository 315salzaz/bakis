//Projektas: Linker4you Front-End
//Vardas Pav: Vilius Kumparskas Povilas Grusas
//Paskirtis: Front_End routing
//Code Line:35
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'log-in-page',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./Pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'l4u',
    loadChildren: () => import('./Pages/redirecting/redirecting.module').then( m => m.RedirectingPageModule)
  },
  {
    path: 'log-in-page',
    loadChildren: () => import('./Pages/log-in-page/log-in-page.module').then( m => m.LogInPagePageModule)
  },
  {
    path: 'signup-page',
    loadChildren: () => import('./Pages/signup-page/signup-page.module').then( m => m.SignupPagePageModule)
  },
  {
    path: 'admin',
    loadChildren: () => import('./Pages/admin/admin.module').then( m => m.AdminPageModule)
  },
  {
    path: 'users-list',
    loadChildren: () => import('./Pages/users-list/users-list.module').then( m => m.UsersListPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
